// this gets run from a separate crate in the terminal. it has only this file
// 
use std::fs::File;
use std::io::{Read, Write};

fn main() {
    eprintln!("try open");
    let mut vec_file = File::open("log:/hi")
        .expect("Failed to open vec file");

    eprintln!("try write");
    vec_file.write(b" Hello")
        .expect("Failed to write to vec:");

    vec_file.write(b" Hello")
        .expect("Failed to write to vec:");

    eprintln!("try read");
    let mut read_into = String::new();
    vec_file.read_to_string(&mut read_into)
        .expect("Failed to read from vec:");
    eprintln!("end read");

    println!("{}", read_into); // olleH ih/
}
